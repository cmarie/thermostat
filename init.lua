gpio.mode(3, gpio.OUTPUT)
gpio.mode(4, gpio.OUTPUT)
gpio.write(3, gpio.LOW)
gpio.write(4, gpio.LOW)
CONTROL=require("control")
NTP=require("ntp")
JSON=require("cjson")
DHT=require("dht")
UDP=require("udpd")
REPORT=require("httpd")
climate={}
redistribution={}
relays={}
device={}
config={}
statistics={
    heating=0,
    cooling=0,
    redistribution=0
}
if file.open("config.json") then
    config = JSON.decode(file.readline())
    file.close()
end
wifi.setmode(wifi.STATION)

wifi.sta.eventMonReg(wifi.STA_IDLE, function() print("STATION_IDLE") end)
wifi.sta.eventMonReg(wifi.STA_CONNECTING, function() print("STATION_CONNECTING") end)
wifi.sta.eventMonReg(wifi.STA_WRONGPWD, function() print("STATION_WRONG_PASSWORD") end)
wifi.sta.eventMonReg(wifi.STA_APNOTFOUND, function() print("STATION_NO_AP_FOUND") end)
wifi.sta.eventMonReg(wifi.STA_FAIL, function() print("STATION_CONNECT_FAIL") end)
wifi.sta.eventMonReg(wifi.STA_GOTIP, function()
    NTP.sync()
    tmr.alarm(0, 1000, 1, function()
        if NTP.timestamp ~= 0 then
            tmr.stop(0)
            device.timestamp = NTP.timestamp
            device.uptime = 0
            device.sensor_errors = 0
            redistribution.last_cycle = 0
            if config.redistribution.enabled then
                redistribution.next_cycle = device.timestamp + config.redistribution.wait
            else
                redistribution.next_cycle = device.timestamp + config.redistribution.wait
            end
            climate.debounce_threshold = 0
            UDP.startServer(config.network.port)
            NTP.timestamp = nil
            NTP.sync = nil
            NTP = nil
            package.loaded["ntp"]=nil
            tmr.alarm(0, config.device.sample_rate, 1, function()
                local s,t,h,td,hd = DHT.read(9)
                if s == 0 then
                    climate.temperature = (t * 1.8 + 32)
                    climate.humidity = h
                    if config.device.enabled then
                        CONTROL.control()
                    end
                else
                    device.sensor_errors = device.sensor_errors + 1
                end
            end)
            tmr.alarm(1, 1000, 1, function()
                if config.redistribution.enabled then
                    if device.timestamp >= redistribution.next_cycle then
                        CONTROL.redistribute()
                    end
                end
                device.timestamp = device.timestamp + 1
                device.uptime = device.uptime + 1
            end)
            tmr.alarm(2, 60000, 1, function()
                if config.reporting.enabled then
                    REPORT.send()
                end
            end)
        end
    end)
end)
wifi.sta.eventMonStart()
wifi.sta.config(config.wifi.ssid, config.wifi.pass)
