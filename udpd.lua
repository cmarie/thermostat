local moduleName = ...
local M = {}
_G[moduleName] = M
M.sv_info = function(s, c)
    local data = {relays={},network={},storage={},memory={}}
    data.climate = climate
    data.redistribution = redistribution
    data.config = config
    data.climate = climate
    data.device = device
    data.statistics = statistics
    data.network.ip,data.network.subnet,data.network.gateway = wifi.sta.getip()
    data.network.mac_address = wifi.sta.getmac()
    data.relays.is_heating = CONTROL.isHeating()
    data.relays.is_cooling = CONTROL.isCooling()
    data.memory.total = 36392
    data.memory.free = node.heap()
    data.memory.used = data.memory.total - data.memory.free
    data.storage.free, data.storage.used, data.storage.total = file.fsinfo()
    s:send(JSON.encode(data))
end
M.sv_recode = function(s, c)
    file.remove("init.lua")
    tmr.alarm(2, 5000, 0, node.restart)
end
M.sv_restart = function(s, c)
    tmr.alarm(2, 2000, 0, node.restart)
    return 200
end
M.sv_heat = function(s, c)
    if CONTROL.isCooling() then
        return 500
    elseif (c.on and CONTROL.isHeating()) or (not c.on and not CONTROL.isHeating()) then
        return 204
    else
        gpio.write(3, (c.on and gpio.HIGH or gpio.LOW))
        if not c.on then
            redistribution.next_cycle = device.timestamp + config.redistribution.wait
            statistics.heating = statistics.heating + 1
        end
    end
    return 200
end
M.sv_cool = function(s, c)
    if CONTROL.isHeating() then
        return 500
    elseif (c.on and CONTROL.isCooling()) or (not c.on and not CONTROL.isCooling()) then
        return 204
    else
        gpio.write(4, (c.on and gpio.HIGH or gpio.LOW))
        if not c.on then
            statistics.cooling = statistics.cooling + 1
        end
    end
    return 200
end
M.sv_power = function(s, c)
    if config.device.enabled ~= c.on then
        config.device.enabled = c.on
        gpio.write(3, gpio.LOW)
        gpio.write(4, gpio.LOW)
        return 200
    end
    return 204
end
M.sv_reporting = function(s, c)
    if config.reporting.enabled ~= c.on then
        config.reporting.enabled = c.on
        return 200
    end
    return 204
end
M.sv_redistribution = function(s, c)
    if config.redistribution.enabled ~= c.on then
        redistribution.next_cycle = device.timestamp + config.redistribution.wait
        config.redistribution.enabled = c.on
        if not c.on then
            gpio.write(4, gpio.LOW)
        end
        return 200
    end
    return 204
end
M.sv_set = function(s, c)
    config = c.data
    if file.open("config.json", "w+") then
        file.write(JSON.encode(c.data))
        file.close()
        return 200
    end
    return 640
end

M.startServer = function(port)
    s=net.createServer(net.UDP)
    s:on("receive",function(s,c)
        c=JSON.decode(c)
        local callback = M[c.cmd](s,c)
        if callback~=nil then
            s:send(JSON.encode({code=callback}))
        end
    end)
    s:listen(port)
end
return M
