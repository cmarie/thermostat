local moduleName = ...
local M = {}
_G[moduleName] = M
M.control = function()
    if (climate.temperature < config.climate.target_temperature) then
        if not M.isHeating() and not M.isCooling() then
            if climate.debounce_threshold >= config.climate.debounce_threshold then
                gpio.write(3, gpio.HIGH)
                climate.debounce_threshold = 0
            end
            climate.debounce_threshold = climate.debounce_threshold + 1
        end
    else
        if M.isHeating() and not M.isCooling() then
            gpio.write(3, gpio.LOW)
            redistribution.next_cycle = device.timestamp+config.redistribution.wait
            statistics.heating = statistics.heating + 1
        end
        climate.debounce_threshold = 0
    end
end
M.redistribute = function()
    if not M.isHeating() and not M.isCooling() then
        gpio.write(4, gpio.HIGH);
        tmr.alarm(3, config.redistribution.duration * 1000, 0, function()
            redistribution.last_cycle = device.timestamp
            redistribution.next_cycle = device.timestamp+config.redistribution.wait
            statistics.redistribution = statistics.redistribution + 1
            gpio.write(4, gpio.LOW)
        end)
    end
end
M.isHeating = function()
    return gpio.read(3) == gpio.HIGH
end
M.isCooling = function()
    return gpio.read(4) == gpio.HIGH
end
return M
