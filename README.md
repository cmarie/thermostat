# ESP8266 ESP-01 Based Thermostat
# Wiring Diagram
![Wiring](http://i.imgur.com/bL7aV92.png)

# Requirements
* ATtiny85 (Optional but recommended)
* 3.3v Voltage Regulator
* ESP8266 ESP-01
* 5v regulated power source (USB cable recommended for versatility)
* DHT11/22
* 10k Resistor

Why is the ATtiny needed? It's only needed if your relay module is active high
because the ESP sets it's GPIO pins high on startup which will trigger both
relay channels causing heating and cooling to engage at the same time. The
ATtiny acts as a smart transistor. If the input pin is high longer than a second
it will be seen as a valid event if it's only high for half a second then it
will be ignored. The ATtiny also acts as a bridge between the ESP's 3.3v and the
power sources 5v this provides the relay with 5v instead of 3.3v as a trigger
which keeps the voltage within the range of the relay modules specs. The ATtiny
also acts as a fail safe preventing heating and cooling from activating at the
same time.

# Installation
* Rename `config.default.json` to `config.json` and edit it as needed
* Upload everything to the ESP8266
* Connect 2-channel relay to GPIO2 and GPIO0
* Connect DHT11/22 DATA pin to the ESP8266's RX pin

## API Commands

**Commands must be sent over UDP**

### sv_info
Returns information about the device

**Syntax:** `sv_info`

### sv_recode
Removes init.lua and restarts the device

**Syntax:** `sv_recode`

### sv_heat
Turns the furnace on/off

**Syntax:** `sv_heat={"on": true|false}`

### sv_cool
Turns the fan on/off

**Syntax:** `sv_cool={"on": true|false}`

### sv_power
Turns the device on/off

**Syntax:** `sv_power={"on": true|false}`

### sv_redistribution
Turns redistribution on/off

**Syntax:** `sv_redistribution={"on": true|false}`

### sv_restart
Turns redistribution on/off

**Syntax:** `sv_restart`

### sv_reporting
Turns reporting on/off

**Syntax:** `sv_reporting={"on": true|false}`

### sv_set
Creates config.json file based on keys and values you send to it

**Syntax:** `sv_set=<json>`