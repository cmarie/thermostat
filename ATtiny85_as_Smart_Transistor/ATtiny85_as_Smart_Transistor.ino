const int in_heating_pin = 3;
const int in_cooling_pin = 4;
const int out_heating_pin = 1;
const int out_cooling_pin = 0;
int last = 0;
void setup() {
     pinMode(in_heating_pin, INPUT);
     pinMode(in_cooling_pin, INPUT);
     
     pinMode(out_heating_pin, OUTPUT);
     pinMode(out_cooling_pin, OUTPUT);
     
     digitalWrite(out_heating_pin, LOW);
     digitalWrite(out_cooling_pin, LOW);
     
     delay(5000);
}

void loop() {
     int ms = millis();
     int duration = (ms - last);
     if(digitalRead(in_heating_pin) == HIGH && digitalRead(in_cooling_pin) == HIGH){
          // Do nothing
     }else if(digitalRead(out_heating_pin) != digitalRead(in_heating_pin) && digitalRead(out_cooling_pin) == LOW){
          if(duration >= 500){
               digitalWrite(out_heating_pin, digitalRead(in_heating_pin));
               last = ms;
          }
     }else if(digitalRead(out_cooling_pin) != digitalRead(in_cooling_pin) && digitalRead(out_heating_pin) == LOW){
          if(duration >= 500){
               digitalWrite(out_cooling_pin, digitalRead(in_cooling_pin));
               last = ms;
          }
     }else{
          last = ms;
     }
}
