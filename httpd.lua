local moduleName = ...
local M = {}
_G[moduleName] = M
M.send = function()
    local sk=net.createConnection(net.TCP, 0)
    sk:on("receive", function(a, b)
        a:close()
        sk = nil
    end)
    sk:dns(config.reporting.host, function(conn, ip)
        local data = string.format("_method=POST&data[Thermostat][temp]=%s&data[Thermostat][humidity]=%s&data[Thermostat][is_heating]=%s&data[Thermostat][is_cooling]=%s&data[Thermostat][timestamp]=%s", climate.temperature,climate.humidity,tostring(CONTROL.isHeating()),tostring(CONTROL.isCooling()),device.timestamp)
        sk:connect(80, ip)
        sk:send("POST " .. config.reporting.path .. " HTTP/1.1\r\n")
        sk:send("Host: " .. config.reporting.host .. "\r\n")
        sk:send("Content-Type: application/x-www-form-urlencoded\r\n")
        sk:send("Content-Length: " .. string.len(data) .. "\r\n")
        sk:send("Connection: close\r\n")
        sk:send("Accept: */*\r\n\r\n")
        sk:send(data)
    end)
end
return M
